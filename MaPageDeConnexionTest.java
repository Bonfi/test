  package projet_Acial;

import java.util.concurrent.TimeUnit;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import pages.googleSearchPageObjects;

public class MaPageDeConnexionTest {
	private static WebDriver driver = null;
	public static void main(String[] args) {
		connect();
		
	}
	
	@Test
	public static void connect() {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
//		driver.get("https://advantageonlineshopping.com/#/");
//		driver.manage().window().maximize();
		
		//Create an instance of our pageObjects -->GoogleSearchPageObjects
		
		MaPageDeConnexion cn = new MaPageDeConnexion(driver);
		AjouterCommandes ajc = new AjouterCommandes(driver);
		
		driver.get("https://advantageonlineshopping.com/#/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		cn.clickMenuUser();
		cn.setTUsername("Acial_test");
		cn.setTPassword("Azerty1");
		cn.clickSubmitButton();
		//cn.ajouterCmd();
		ajc.ajouterCmd();
		
		//Ajout Commandes
		cn.buy_it();
		cn.incrementer();
		ajc.verspanier();
	}

}
 