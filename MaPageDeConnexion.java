package projet_Acial;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import com.github.dockerjava.api.model.Driver;

public class MaPageDeConnexion {
	WebDriver driver = null;
	
	
	//Mes selecteurs
	By button_user = By.id("menuUser");
	By name = By.name("username");
	By password = By.name("password");
	By button_submit = By.id("sign_in_btnundefined");
	
	By select_speaker = By.id("speakersImg");
	//------Ajout commandes---------//
	By buy_now = By.name("buy_now");
	By incrementer = By.className("plus");
			
	/*
	 * driver.get("https://advantageonlineshopping.com/#/");
	 * driver.findElement(By.id("menuUser")).click();
	 * driver.findElement(By.name("username")).clear();
	 * driver.findElement(By.name("username")).sendKeys("Acial_test2");
	 * driver.findElement(By.name("password")).click();
	 * driver.findElement(By.name("password")).clear();
	 * driver.findElement(By.name("password")).sendKeys("Azerty1");
	 * driver.findElement(By.id("sign_in_btnundefined")).click();
	 */
	
	//Mes methodes 
	public MaPageDeConnexion(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickMenuUser() {
		driver.findElement(button_user).click();
	}
	public void setTUsername(String text) {
		driver.findElement(name).sendKeys(text);
		
	}
	public void setTPassword(String text) {
		driver.findElement(password).sendKeys(text);
	}
	
	public void clickSubmitButton() {
		driver.findElement(button_submit).click();
		
		//or recommended method
		//driver.findElement(button_submit).sendKeys(Keys.RETURN);
	}

	/*
	 * public void ajouterCmd() { driver.findElement(select_speaker).click(); }
	 */
	//------Ajout commandes
	public void buy_it() {
		driver.findElement(buy_now).click();
	}	
	
	public void incrementer() {
		driver.findElement(incrementer).click();;
	}

}
